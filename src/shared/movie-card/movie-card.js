import React from 'react';
import { StyleSheet, Text, View, Image } from 'react-native';

export default class App extends React.Component {
  render() {
    var movie = this.props.movie;
    
    return (
      <View style={[styles.container,
                   this.props.styles
      ]}>
        
        <View style={styles.flex}>
          <Text 
            style={styles.titleConfig}>{movie.title}</Text>
          
          <Text>{movie.year}</Text>
        </View>
        
        <View>
          <Image
            resizeMode={'cover'} 
            style={{width: 300, height: 100}}
            source={{uri: movie.imageURL}}
          />
        </View>

        <View>
          <Text style={{
            padding: 30,
            paddingTop: 10
          }}>
            {movie.overview}
            </Text>
        </View>  

      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    paddingTop: 16,
  },
  titleConfig: {
      fontSize: 20,
      color: "red",
      paddingBottom: 8
  },
  flex:{
    flex:.2,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center"
  }
});
