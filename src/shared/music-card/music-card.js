import React from 'react';
import { StyleSheet, Text, View, Image } from 'react-native';

export default class MusicCard extends React.Component {
  render() {    
    var music = this.props.music;
    
    return (
      <View>
        <View>
            <Image
            resizeMode={'cover'}
            style={{width: 335, height: 350}}
            source={{uri: music.MusicUrl}} />
        </View>

        <View style={{paddingTop: 10}}>
            <Text style={styles.textTitle}>{music.Title}</Text>
        </View>

        <View style={{paddingTop: 10}}>
            <Text style={styles.textBody}>{music.Body}</Text>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
    textTitle: {
        color: '#fff',
        width: 360,
        textAlign: "center",
        fontWeight: 'bold'
    },
    textBody: {
        color: '#fff',
        width: 360,
        textAlign: "left"
    }
});
