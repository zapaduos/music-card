import React from 'react';
import { StyleSheet, Text, View, Image, ScrollView, FlatList } from 'react-native';
import MusicCard from './src/shared/music-card/music-card';

export default class App extends React.Component {
  render() {
    var music = [{
      MusicUrl: 'https://zinecultural.com/site/uploads/mlib-uploads/full/furacao2000-tornadomuitonervoso22528frente2529-58f68075546ce.jpg',
      Title: 'Furacão 2000',
      Body: 'Os hits das músicas prediletas do professor estão todas aqui! Foto: CD Furacão 2000'
    },
    {
      MusicUrl: 'https://upload.wikimedia.org/wikipedia/pt/thumb/5/54/Musicasmamonasassassinas.jpg/220px-Musicasmamonasassassinas.jpg',
      Title: 'Mamonas Assassinas',
      Body: 'Melhor banda de rock dos anos 90, se viessem nesta maldita geração mimimi, nunca teria dado certo!!!'
    }]

    return (
      <View style={styles.container}>
          <FlatList
          horizontal={true}
          data={music}
          renderItem={({item}) => <MusicCard music={item}/>}
          />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "column",
    margin: 'auto',
    padding: 20,
    paddingTop :16,
    backgroundColor: '#000000'
}
});
